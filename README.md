# Summary
This project is a sample web application to render 'Top 5 Websites Ranking' report. 

## Motivation
To accomplish the assignment given by TM R&D.

## Installing
	1. Install virtualenv
    * [sudo] pip install virtualenv

	2. Activate virtualenv
	* source venv/bin/activate

	3. Go to 'source' folder and install requirements
	* pip install -r requirements.txt
    
    4. Migration and model verification
    * python manage.py makemigrations websites
    * python manage.py migrate
    
    5. Create superuser
    * python manage.py createsuperuser

## Running
	1. Go to source folder and run the following command:
	   python manage.py runserver
	
	2. From your Chrome browser go to "127.0.0.1:8000"
    
### Admin Page
	1. From your browser go to "127.0.0.1:8000/admin"