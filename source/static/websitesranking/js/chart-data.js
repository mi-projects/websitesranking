var loadData = function (label, data) {
    var option = {
    responsive: true,
    };
    
    // Get the context of the canvas element we want to select
    var ctx = document.getElementById("myChart").getContext('2d');
    var data = {
        labels: label,
        datasets: [
            {
                label: "Top 5 Websites Ranking",
                fillColor: "rgba(151,187,205,0.5)",
                strokeColor: "rgba(151,187,205,0.8)",
                highlightFill: "rgba(151,187,205,0.75)",
                highlightStroke: "rgba(151,187,205,1)",
                data: data
            }
        ]
    };

    var myBarChart = new Chart(ctx).Bar(data, option);
    
};

