# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from websites.models import Website


# Register your models here.
admin.site.register(Website)