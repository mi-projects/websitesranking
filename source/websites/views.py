# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, get_object_or_404, redirect, render_to_response
from django.template import RequestContext
from django.http import HttpResponse
import dateutil.parser
from django.http import JsonResponse
from django.core.files.storage import FileSystemStorage
from django.conf import settings
import csv
from .models import Website

MEDIA_ROOT = getattr(settings, "MEDIA_ROOT", None)

# Create your views here.

def index(request):
    """
    GET/POST Home Page - Dashboard
    """
    if request.method == 'GET':
	   return render(request, 'home.html', {})
    else:
        date_str = request.POST.get('select','')
        date = dateutil.parser.parse(date_str)
        web = Website.objects.filter(date=date).order_by('-visits')[:5]
        name = []
        visits = []
        if web.count() == 0:
            return JsonResponse({'msg': 'no info', 'name': name, 'visits': visits}) 
        
        for w in web:
            name.append(w.name)
            visits.append(w.visits)
        
        return JsonResponse({'name': name, 'visits': visits})
        #return render(request, 'home.html', {'top5': web})

def upload(request): 
    """
    GET/POST Upload Page
    """
    if request.method == 'POST' and request.FILES['file']:
        myfile = request.FILES['file']
        fs = FileSystemStorage()
        filename = fs.save(myfile.name, myfile)
        uploaded_file_url = fs.url(filename)
        
        with open(MEDIA_ROOT + '/' + filename) as f:
            reader = csv.reader(f, delimiter=str(u'|'))
            next(reader)
            
            for row in reader:
                _, created = Website.objects.get_or_create(
                    name=row[1],
                    visits=row[2],
                    date=dateutil.parser.parse(row[0]),
                )
        
        return render(request, 'upload-csv.html', {
            'uploaded_file_url': uploaded_file_url
        })
    return render(request, 'upload-csv.html')